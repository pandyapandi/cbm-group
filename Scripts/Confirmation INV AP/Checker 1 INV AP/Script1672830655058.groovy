import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.click(findTestObject('Object Repository/Checker Conf INVAP/button_Close'))

WebUI.setText(findTestObject('Object Repository/Checker Conf INVAP/input_Login to your account_ident'), GlobalVariable.pempek_cc1)

WebUI.setEncryptedText(findTestObject('Object Repository/Checker Conf INVAP/input_Login to your account_identk'), GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/Checker Conf INVAP/button_Login'))

WebUI.click(findTestObject('Object Repository/Checker Conf INVAP/a_Invoice AP'))

WebUI.click(findTestObject('Object Repository/Checker Conf INVAP/a_Invoice Checker'))

WebUI.setText(findTestObject('Object Repository/Checker Conf INVAP/input_Search_filterRow'), GlobalVariable.invno)

WebUI.click(findTestObject('Object Repository/Checker Conf INVAP/button_Search'))

WebUI.click(findTestObject('Object Repository/Checker Conf INVAP/a_Details'))

WebUI.click(findTestObject('Object Repository/Checker Conf INVAP/button_Approve'))

WebUI.setText(findTestObject('Object Repository/Checker Conf INVAP/textarea_Note_note_approve'), 'CC1')

WebUI.click(findTestObject('Object Repository/Checker Conf INVAP/button_Approve_1'))

WebUI.click(findTestObject('Object Repository/Checker Conf INVAP/a_Checker Pempek 5'))

WebUI.click(findTestObject('Object Repository/Checker Conf INVAP/a_Logout'))

WebUI.click(findTestObject('Object Repository/Checker Conf INVAP/a_Logout_1'))

WebUI.closeBrowser()

