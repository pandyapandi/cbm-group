import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.click(findTestObject('Object Repository/Maker Conf INVAP PU/button_Close'))

WebUI.setText(findTestObject('Object Repository/Maker Conf INVAP PU/input_Login to your account_ident'), GlobalVariable.mango_mp)

WebUI.setEncryptedText(findTestObject('Object Repository/Maker Conf INVAP PU/input_Login to your account_identk'), GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/Maker Conf INVAP PU/button_Login'))

WebUI.click(findTestObject('Object Repository/Maker Conf INVAP PU/a_Invoice AP'))

WebUI.click(findTestObject('Object Repository/Maker Conf INVAP PU/a_Inbox Maker'))

WebUI.setText(findTestObject('Object Repository/Maker Conf INVAP PU/input_Search_confirm_filterRow'), GlobalVariable.invno)

WebUI.click(findTestObject('Object Repository/Maker Conf INVAP PU/button_Search'))

WebUI.click(findTestObject('Object Repository/Maker Conf INVAP PU/a_Details'))

WebUI.click(findTestObject('Object Repository/Maker Conf INVAP PU/button_Process'))

WebUI.setText(findTestObject('Object Repository/Maker Conf INVAP PU/textarea__note_approve'), 'MC1')

WebUI.click(findTestObject('Object Repository/Maker Conf INVAP PU/button_Approve'))

WebUI.click(findTestObject('Object Repository/Maker Conf INVAP PU/a_Maker Mango'))

WebUI.click(findTestObject('Object Repository/Maker Conf INVAP PU/a_Logout'))

WebUI.click(findTestObject('Object Repository/Maker Conf INVAP PU/a_Logout_1'))

WebUI.closeBrowser()

