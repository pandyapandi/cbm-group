import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.click(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/button_Close'))

WebUI.setText(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/input_Login to your account_ident'), 
    GlobalVariable.mango_s4)

WebUI.setEncryptedText(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/input_Login to your account_identk'), 
    GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/button_Login'))

WebUI.click(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/a_Invoice AP'))

WebUI.click(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/a_Invoice Signer'))

WebUI.setText(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/input_Search_filterRow'), GlobalVariable.invno)

WebUI.click(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/button_Search'))

WebUI.click(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/a_Details'))

WebUI.click(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/button_Approve'))

WebUI.setText(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/textarea_Note_note_approve'), 
    'S4')

WebUI.setEncryptedText(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/input_Clickto get your OTP code_otp_code'), 
    'SFTQUhjBfIY=')

WebUI.click(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/button_Approve_1'))

WebUI.click(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/a_Signer Mango'))

WebUI.click(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/a_Logout'))

WebUI.click(findTestObject('Object Repository/Signer_INVAP/Page_Supply Chain Management/a_Logout_1'))

WebUI.closeBrowser()

