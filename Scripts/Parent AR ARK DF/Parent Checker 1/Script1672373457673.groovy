import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Parent Checker/button_Close'))

WebUI.setText(findTestObject('Object Repository/Parent Checker/input_Login to your account_ident'), GlobalVariable.pempek_pc1)

WebUI.setEncryptedText(findTestObject('Object Repository/Parent Checker/input_Login to your account_identk'), GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/Parent Checker/button_Login'))

WebUI.click(findTestObject('Object Repository/Parent Checker/a_MakerMango Checkerpempek'))

WebUI.click(findTestObject('Object Repository/Parent Checker/a_Checker PempekPempek 123Active'))

WebUI.click(findTestObject('Object Repository/Parent Checker/a_Invoice AR'))

WebUI.click(findTestObject('Object Repository/Parent Checker/a_Invoice Checker'))

WebUI.click(findTestObject('Object Repository/Parent Checker/a_Approval as a Parent'))

WebUI.setText(findTestObject('Parent Checker/input_Search_filterRow'), GlobalVariable.invno)

WebUI.click(findTestObject('Parent Checker/button_Search'))

WebUI.click(findTestObject('Object Repository/Parent Checker/a_Details'))

WebUI.click(findTestObject('Object Repository/Parent Checker/button_Process'))

WebUI.setText(findTestObject('Object Repository/Parent Checker/textarea_Note_note_approve'), 'PC1')

WebUI.click(findTestObject('Object Repository/Parent Checker/button_Approve'))

WebUI.click(findTestObject('Object Repository/Parent Checker/a_Checker Pempek'))

WebUI.click(findTestObject('Object Repository/Parent Checker/a_Logout'))

WebUI.click(findTestObject('Object Repository/Parent Checker/a_Logout_1'))

WebUI.closeBrowser()

