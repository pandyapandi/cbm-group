import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/button_Close'))

WebUI.setText(findTestObject('Object Repository/Maker PU INVAR/input_Login to your account_ident'), GlobalVariable.pempek_mp)

WebUI.setEncryptedText(findTestObject('Object Repository/Maker PU INVAR/input_Login to your account_identk'), GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/button_Login'))

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/a_SignerMango MakerPempek'))

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/a_Maker PempekPempek 123Active'))

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/a_Invoice AR'))

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/a_New Invoice'))

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/div_Single Entry InvoiceInput single data invoice'))

//WebUI.click(findTestObject('Object Repository/Maker PU INVAR/div_Single Entry InvoiceInput single data invoice_1'))

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/span_- Select Company -'))

WebUI.setText(findTestObject('Object Repository/Maker PU INVAR/input_Close_select2-search__field'), GlobalVariable.anchor)

WebUI.sendKeys(findTestObject('Object Repository/Maker PU INVAR/input_Close_select2-search__field'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/span_- Select Payment Method -'))

WebUI.setText(findTestObject('Object Repository/Maker PU INVAR/input_Close_select2-search__field'), GlobalVariable.paytot)

WebUI.sendKeys(findTestObject('Object Repository/Maker PU INVAR/input_Close_select2-search__field'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Maker PU INVAR/input__amount'), GlobalVariable.amount)

Date today = new Date()

String currentDate = today.format('dd-MM-yyyy')

String invDate = today.format('ddMMyyyy')

String invTime = today.format('hhmmss')

disburse = today

settle = (today + 1)

sharing = (today + 1)

String disbursementDate = disburse.format('dd-MM-yyyy')

String settlementDate = settle.format('dd-MM-yyyy')

String sharinglimitDate = settle.format('dd-MM-yyyy')

String noInv = (GlobalVariable.invno)

println(noInv)

println(currentDate)

println(settlementDate)

WebUI.setText(findTestObject('Object Repository/Maker PU INVAR/input__payment_date'), disbursementDate)

WebUI.setText(findTestObject('Object Repository/Maker PU INVAR/input__maturity_date'), settlementDate)

WebUI.setText(findTestObject('Object Repository/Maker PU INVAR/input__entitas_inv_no'), (GlobalVariable.invno +
	invDate) + invTime)

WebUI.setText(findTestObject('Object Repository/Maker PU INVAR/input__notes'), (GlobalVariable.invno +
	invDate) + invTime)

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/button_Submit'))

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/a_Maker Pempek'))

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/a_Logout'))

WebUI.click(findTestObject('Object Repository/Maker PU INVAR/a_Logout_1'))

WebUI.closeBrowser()

