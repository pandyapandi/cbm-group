import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Checker/Page_Supply Chain Management/button_Close'))

WebUI.setText(findTestObject('Checker/Page_Supply Chain Management/input_Login to your account_ident'), 
    GlobalVariable.mango_c2)

WebUI.setEncryptedText(findTestObject('Checker/Page_Supply Chain Management/input_Login to your account_identk'), 
    GlobalVariable.password)

WebUI.click(findTestObject('Checker/Page_Supply Chain Management/button_Login'))

WebUI.click(findTestObject('Checker/Page_Supply Chain Management/a_Invoice AR'))

WebUI.click(findTestObject('Checker/Page_Supply Chain Management/a_Invoice Checker'))

WebUI.setText(findTestObject('Checker/Page_Supply Chain Management/input_Search_filterRow'), GlobalVariable.invno)

WebUI.click(findTestObject('Checker/Page_Supply Chain Management/button_Search'))

WebUI.click(findTestObject('Checker/Page_Supply Chain Management/a_Details'))

WebUI.click(findTestObject('Checker/Page_Supply Chain Management/button_Process'))

WebUI.setText(findTestObject('Checker/Page_Supply Chain Management/textarea_Note_note_approve'), 'C2')

WebUI.click(findTestObject('Checker/Page_Supply Chain Management/button_Approve'))

WebUI.click(findTestObject('Checker/Page_Supply Chain Management/a_Checker Mango'))

WebUI.click(findTestObject('Checker/Page_Supply Chain Management/a_Logout'))

WebUI.click(findTestObject('Checker/Page_Supply Chain Management/i_Logout_icon-switch2 font-size-base mr-1'))

WebUI.closeBrowser()

