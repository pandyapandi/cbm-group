import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.url)

WebUI.click(findTestObject('Object Repository/Checker Conf Parent INVAR/button_Close'))

WebUI.setText(findTestObject('Object Repository/Checker Conf Parent INVAR/input_Login to your account_ident'), GlobalVariable.pempek_cpar1)

WebUI.setEncryptedText(findTestObject('Object Repository/Checker Conf Parent INVAR/input_Login to your account_identk'), 
    GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/Checker Conf Parent INVAR/button_Login'))

WebUI.click(findTestObject('Object Repository/Checker Conf Parent INVAR/a_Invoice AR'))

WebUI.click(findTestObject('Object Repository/Checker Conf Parent INVAR/a_Invoice Checker'))

WebUI.click(findTestObject('Object Repository/Checker Conf Parent INVAR/a_Approval as a Parent'))

WebUI.setText(findTestObject('Object Repository/Checker Conf Parent INVAR/input_Search_filterRow'), GlobalVariable.invno)

WebUI.click(findTestObject('Object Repository/Checker Conf Parent INVAR/button_Search'))

WebUI.click(findTestObject('Object Repository/Checker Conf Parent INVAR/a_Details'))

WebUI.click(findTestObject('Object Repository/Checker Conf Parent INVAR/button_Process'))

WebUI.setText(findTestObject('Page_Supply Chain Management/textarea_Note_note_approve'), 'CPPU1')

WebUI.click(findTestObject('Object Repository/Checker Conf Parent INVAR/button_Approve'))

WebUI.click(findTestObject('Object Repository/Checker Conf Parent INVAR/a_Checker Rans'))

WebUI.click(findTestObject('Object Repository/Checker Conf Parent INVAR/a_Logout'))

WebUI.click(findTestObject('Object Repository/Checker Conf Parent INVAR/i_Logout_icon-switch2 font-size-base mr-1'))

WebUI.closeBrowser()

