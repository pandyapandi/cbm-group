<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>APPROVEC1S1-PC1S1-INVAP-AP</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>4e6a91ab-16e0-4da3-b0b6-6ed7a3b846ff</testSuiteGuid>
   <testCaseLink>
      <guid>f6d5623e-9031-4d0b-8bee-fd50667e518f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Maker SCF AP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10a9c935-ce3e-40e7-b572-599ea1766734</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Checker 1 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db428c54-3cbb-487a-a453-209c6aad0ed5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Signer 1 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>053fe15a-29be-4fda-a1ef-49b7b8a2e0aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent AP VF/Parent Checker 1 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14e959e7-027b-4090-96f8-d3d82cb7a1da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent AP VF/Parent Signer 1 INVAP</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
