<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>APPROVEC1S1-PC1S1-INVAR-AR</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>44882aa4-48a9-4039-a595-5ef9eb54b4d3</testSuiteGuid>
   <testCaseLink>
      <guid>29fd733f-04cf-4848-a7cb-750f70d83004</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Maker SCF AR ARK</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1591bc7-dabc-4c86-813c-c39cddb419b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Checker 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c5c0124-206d-4462-b263-208a742f0506</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Signer 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>522d1296-ed75-43cf-8052-48f519aa8523</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent AR ARK DF/Parent Checker 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50f7ec0c-0baa-4593-8653-b535a6de2379</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent AR ARK DF/Parent Signer 1</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
