<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>APPROVEC1S1-PC1S1-INVAR-AR-CONF-MC1S1 - CANCEL</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>4138bc31-b86f-4ea4-a9dc-503217118018</testSuiteGuid>
   <testCaseLink>
      <guid>29fd733f-04cf-4848-a7cb-750f70d83004</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Maker SCF AR ARK</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1591bc7-dabc-4c86-813c-c39cddb419b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Checker 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c5c0124-206d-4462-b263-208a742f0506</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Signer 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>522d1296-ed75-43cf-8052-48f519aa8523</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent AR ARK DF/Parent Checker 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50f7ec0c-0baa-4593-8653-b535a6de2379</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent AR ARK DF/Parent Signer 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acfd3d85-6320-4836-bc03-57b27b04e4cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Maker 1 INV AP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d949820-7517-43a3-b343-09407c8c2631</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Checker 1 INV AP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1439a14-77d7-46b6-9ef8-bc9465e8405d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Signer 1 INV AP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e420dc84-9f7b-4da6-92d3-2f65d11b4622</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Parent Checker 1 INV AP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99c15d88-f829-433c-8f3e-f22643bbcc81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Parent Signer 1 INV AP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a90280b3-ae33-4b12-b673-2d8aed594f0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cancel Invoice/Maker Cancel AR ARK DF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15541842-09c6-467b-abbc-959cab8f4d1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cancel Invoice/Checker Cancel AR ARK DF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f5c0352-e300-438f-8fd8-2e80002af5f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cancel Invoice/Signer Cancel AR ARK DF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29b04523-7142-4adb-b203-171affcbce2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cancel Invoice/Checker Cancel Parent AR ARK DF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a304143-2029-45ce-aa85-4ff5e78d4dab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cancel Invoice/Signer Cancel Parent AR ARK DF</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
