<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>APPROVEC5S5-INVAP-AP</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>59b7fd1e-0a3b-4d80-b055-2dfb6fc85392</testSuiteGuid>
   <testCaseLink>
      <guid>f6d5623e-9031-4d0b-8bee-fd50667e518f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Maker SCF AP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10a9c935-ce3e-40e7-b572-599ea1766734</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Checker 1 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>262f636f-f845-4cd7-96e5-fe7e9e48c7f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Checker 2 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4b71ac28-10e2-4f1e-aee7-f865f3358534</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Checker 3 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8ac6eb7-44c5-4663-b4a8-b23dbcc0d63f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Checker 4 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35ead346-2a17-4c97-8a29-b60cf968f2b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Checker 5 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db428c54-3cbb-487a-a453-209c6aad0ed5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Signer 1 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>120226da-fa25-4836-bcef-d16df355f17d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Signer 2 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9793f89d-0e63-47f6-946b-86b843604623</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Signer 3 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7beebec5-ac27-46fb-bc7a-9117fe454447</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Signer 4 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>706d0795-5539-4112-a9ca-a8242e1e5f48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/INV AP/Signer 5 INVAP</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
