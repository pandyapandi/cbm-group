<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>APPROVEC1S1-PC1S1-INVAP-VF</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>0ff169f8-1b10-430f-a1d2-e28e5a368755</testSuiteGuid>
   <testCaseLink>
      <guid>b584e011-35f9-4a3f-bb9f-b4857d71fc2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Maker VF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10a9c935-ce3e-40e7-b572-599ea1766734</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Checker 1 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db428c54-3cbb-487a-a453-209c6aad0ed5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AP/Signer 1 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b439944a-5b2d-4c21-a109-21c352e72327</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent AP VF/Parent Checker 1 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4f55472-8b7b-4d42-aac7-184a86902b9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent AP VF/Parent Signer 1 INVAP</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
