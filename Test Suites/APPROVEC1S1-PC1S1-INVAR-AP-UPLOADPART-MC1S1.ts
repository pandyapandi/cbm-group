<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>APPROVEC1S1-PC1S1-INVAR-AP-UPLOADPART-MC1S1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>768f5aa5-cc23-4f4a-b635-50598255616e</testSuiteGuid>
   <testCaseLink>
      <guid>d7c9df06-5132-409a-9b77-51e1a575d890</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/INV AR/Maker SCF AP PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa1a106e-2915-4417-9fb7-f674cb1e19c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/INV AR/Checker SCF AP VF PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>671de93f-3389-4cce-8044-f48955b5e843</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/INV AR/Signer SCF AP VF PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3e7f379-1814-4221-97b0-ee0c1c84215f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent AP VF/Checker Conf Parent 1 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b3aae9d-7da1-4a59-9949-22f4949f1280</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Parent AP VF/Signer Conf Parent 1 INVAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6962ff6f-f99a-4342-9bdf-5340992c9405</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Maker 1 INV AP PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae3af7f5-022d-436d-bfda-8d256e39ddf8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Checker 1 INV AP PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7e92971-99b4-4de2-87d1-91641c5915d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Signer 1 INV AP PU</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
