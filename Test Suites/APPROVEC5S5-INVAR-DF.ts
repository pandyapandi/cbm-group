<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>APPROVEC5S5-INVAR-DF</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>fc683ee2-8515-4c46-9554-609c01f60ae1</testSuiteGuid>
   <testCaseLink>
      <guid>66f50f36-464e-4501-84c5-ebb77b413370</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/INV AR/Maker DF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d6ddf8e-6f25-4b66-8ec1-c0d6ea97c09a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Checker 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92b7f3ea-74f3-4a1b-a3cc-e129b79d2dc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Checker 2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eac6d57a-983d-4985-8908-d17c3fad9a47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Checker 3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>79369829-a9d9-4129-81ba-ed3ffcfd9d9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Checker 4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2657dc57-e8d2-4c7f-9bd5-a67eaa47ddd1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Checker 5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>214a198c-0a22-4dfc-81c1-fb33b22618d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Signer 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e937ede-89dc-4527-aeca-0a35e5a21a86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Signer 2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>982fc041-0768-4f78-8530-6523f3ff4bbe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Signer 3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d540ded5-bce4-4e06-925e-80b3c66c9b1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Signer 4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8ba2f44-c545-4cfe-8800-49a0efa22c99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Signer 5</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
