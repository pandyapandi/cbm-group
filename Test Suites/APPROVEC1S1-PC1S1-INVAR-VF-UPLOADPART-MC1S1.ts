<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>APPROVEC1S1-PC1S1-INVAR-VF-UPLOADPART-MC1S1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>a48ec010-b2e4-4188-900e-da02edb4a1cd</testSuiteGuid>
   <testCaseLink>
      <guid>8cd53336-3f51-4163-8976-a601db16849e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/INV AR/Maker VF PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa1a106e-2915-4417-9fb7-f674cb1e19c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/INV AR/Checker SCF AP VF PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>671de93f-3389-4cce-8044-f48955b5e843</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/INV AR/Signer SCF AP VF PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3cd8690-3a2d-4116-ac35-384cb3b3349d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Maker 1 INV AP PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c24b000-887e-479a-a05e-35ae8e08a66c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Checker 1 INV AP PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1140a735-d960-4a1f-b7c5-362f505f9800</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Signer 1 INV AP PU</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb1c34dc-ba34-4a03-b49b-74a81ccb6048</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AR/Checker 1 Conf INV AR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d15644b-34e2-457e-843b-5457fe00c4a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AR/Signer 1 Conf INV AR</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
