<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>APPROVEC1S1-PC1S1-INVAR-DF-CONF-MC1S1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>07183835-a372-43be-88b2-d8797c3f5111</testSuiteGuid>
   <testCaseLink>
      <guid>5139d098-0f85-4404-91c7-c88ce3def4b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Maker DF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1591bc7-dabc-4c86-813c-c39cddb419b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Checker 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c5c0124-206d-4462-b263-208a742f0506</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/INV AR/Signer 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>522d1296-ed75-43cf-8052-48f519aa8523</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent AR ARK DF/Parent Checker 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50f7ec0c-0baa-4593-8653-b535a6de2379</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Parent AR ARK DF/Parent Signer 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acfd3d85-6320-4836-bc03-57b27b04e4cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Maker 1 INV AP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d949820-7517-43a3-b343-09407c8c2631</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Checker 1 INV AP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1439a14-77d7-46b6-9ef8-bc9465e8405d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Confirmation INV AP/Signer 1 INV AP</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
